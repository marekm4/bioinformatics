stop_codons = ['UAA', 'UAG', 'UGA']

codons = {
        'UUU': 'F',
        'CUU': 'L',
        'AUU': 'I',
        'GUU': 'V',
        'UUC': 'F',
        'CUC': 'L',
        'AUC': 'I',
        'GUC': 'V',
        'UUA': 'L',
        'CUA': 'L',
        'AUA': 'I',
        'GUA': 'V',
        'UUG': 'L',
        'CUG': 'L',
        'AUG': 'M',
        'GUG': 'V',
        'UCU': 'S',
        'CCU': 'P',
        'ACU': 'T',
        'GCU': 'A',
        'UCC': 'S',
        'CCC': 'P',
        'ACC': 'T',
        'GCC': 'A',
        'UCA': 'S',
        'CCA': 'P',
        'ACA': 'T',
        'GCA': 'A',
        'UCG': 'S',
        'CCG': 'P',
        'ACG': 'T',
        'GCG': 'A',
        'UAU': 'Y',
        'CAU': 'H',
        'AAU': 'N',
        'GAU': 'D',
        'UAC': 'Y',
        'CAC': 'H',
        'AAC': 'N',
        'GAC': 'D',
        'CAA': 'Q',
        'AAA': 'K',
        'GAA': 'E',
        'CAG': 'Q',
        'AAG': 'K',
        'GAG': 'E',
        'UGU': 'C',
        'CGU': 'R',
        'AGU': 'S',
        'GGU': 'G',
        'UGC': 'C',
        'CGC': 'R',
        'AGC': 'S',
        'GGC': 'G',
        'CGA': 'R',
        'AGA': 'R',
        'GGA': 'G',
        'UGG': 'W',
        'CGG': 'R',
        'AGG': 'R',
        'GGG': 'G',
    }

reversed_codons = {}
for codon in codons:
    if codons[codon] not in reversed_codons:
        reversed_codons[codons[codon]] = [codon]
    else:
        reversed_codons[codons[codon]].append(codon)

protein = input()

count = 1
for i in protein:
    count *= len(reversed_codons[i])
    count %= 1000000
count *= len(stop_codons)
count %= 1000000

print(count)
