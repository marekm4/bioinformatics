counts = {
    "A": 0,
    "C": 0,
    "G": 0,
    "T": 0,
}

text = input()

for l in text:
    counts[l] += 1

print(counts['A'], counts['C'], counts['G'], counts['T'], sep=" ")