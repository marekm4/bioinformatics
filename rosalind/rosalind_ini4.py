line = input()
split = line.split(" ")
a = int(split[0])
b = int(split[1])

sum = 0
for i in range(a, b+1):
    if i % 2 == 1:
        sum += i

print(sum)