s = input()
t = input()

def hamming(s, t):
    distance = 0
    for i in range(0, len(s) - 1):
        if s[i] != t[i]:
            distance += 1
    return distance

print(hamming(s, t))