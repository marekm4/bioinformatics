import sys


def read_FASTA(file):
    DNAs = {}
    label = ''
    DNA = ''
    for line in file:
        if line[0] == '>':
            if label != '':
                DNAs[label] = DNA
                DNA = ''
            label = str(line[1:]).rstrip()
        else:
            DNA += line.rstrip()
    DNAs[label] = DNA
    return DNAs

DNAs = read_FASTA(sys.stdin)

min = len(DNAs[list(DNAs.keys())[0]])
for DNA in DNAs:
    l = len(DNAs[DNA])
    if l < min:
        min = l;

motif_found = False
for k in range(min, 0, -1):
    for DNA in DNAs:
        val = DNAs[DNA]
        for i in range(0, len(val) - k + 1):
            motif = val[i:i+k]
            found = True
            for DNA in DNAs:
                if motif not in DNAs[DNA]:
                    found = False
                    break
            if found:
                print(motif)
                motif_found = True
                break
        if motif_found:
            break
    if motif_found:
        break