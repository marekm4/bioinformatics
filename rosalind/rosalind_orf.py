import sys
import rosalind

dna = rosalind.read_fasta(sys.stdin)
dna_reversed_complement = rosalind.reverse_complement(dna.sequence)
rna = rosalind.dna_to_rna(dna.sequence)
rna_reversed_complement = rosalind.dna_to_rna(dna_reversed_complement)

proteins = []
for i in range(0, len(rna) - 2):
    codon = rna[i:i+3]
    if rosalind.is_start_codon(codon):
        try:
            protein = rosalind.rna_to_protein(rna[i:])
            proteins.append(protein)
        except ValueError as e:
            print(e, file=sys.stderr)


for i in range(0, len(rna_reversed_complement) - 2):
    codon = rna_reversed_complement[i:i+3]
    if rosalind.is_start_codon(codon):
        try:
            protein = rosalind.rna_to_protein(rna_reversed_complement[i:])
            proteins.append(protein)
        except ValueError as e:
            print(e, file=sys.stderr)

proteins = set(proteins)
for p in proteins:
    print(p)
