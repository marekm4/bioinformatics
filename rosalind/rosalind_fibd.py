import numpy as np

args = input().split(" ")
n = int(args[0])
m = int(args[1])

populations = np.zeros((n, m), int)
populations[0][0] = 1

for i in range(1, n):
    last_population = populations[i-1]
    current_population = populations[i]
    current_population[0] = np.sum(last_population[1:])
    for j in range(1, m):
        current_population[j] = last_population[j-1]

print(np.sum(populations[-1]))