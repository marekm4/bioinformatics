import sys
import os.path
import urllib.request
import re

class Fasta:
    def __init__(self, name, label, sequence):
        self.name = name
        self.label = label
        self.sequence = sequence
    def __str__(self):
        return '{}: {}: {}'.format(self.name, self.label, self.sequence)

def read_fasta(file, name):
    sequence = ''
    for line in file:
        if line[0] == '>':
            label = str(line[1:]).rstrip()
        else:
            sequence += line.rstrip()
    return Fasta(name, label, sequence)


def read_proteins(name):
    filename = 'cache/{}.fasta'.format(name)

    if not os.path.isfile(filename):
        fileurl = 'http://www.uniprot.org/uniprot/{}.fasta'.format(name)
        urllib.request.urlretrieve(fileurl, filename)

    file = open(filename, 'r')
    protein = read_fasta(file, name)
    file.close()
    return protein

# To allow for the presence of its varying forms, a protein motif is represented by a shorthand as follows: [XY] means "either X or Y" and {X} means "any amino acid except X." For example, the N-glycosylation motif is written as N{P}[ST]{P}.
N_glycosylation = re.compile('(?=(N[^P](S|T)[^P]))')

for line in sys.stdin:
    line = line.rstrip()
    protein = read_proteins(line)
    findall = list(N_glycosylation.finditer(protein.sequence))
    if len(findall) > 0:
        print(protein.name)
        print(' '.join([str(f.start() + 1) for f in findall]))