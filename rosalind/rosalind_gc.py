import sys
import operator

DNAs = {}

label = ''
GCs = 0
total = 0
for line in sys.stdin:
    if line[0] == '>':
        # save current
        if label != '':
            DNAs[label] = 100 * GCs / total
        label = str(line[1:]).rstrip()
        GCs = 0
        total = 0
    else:
        for n in line.rstrip():
            if n == 'C' or n == 'G':
                GCs += 1
            total += 1
DNAs[label] = 100 * GCs / total

max_label = max(DNAs.items(), key=operator.itemgetter(1))[0]
print(max_label)
print(DNAs[max_label])