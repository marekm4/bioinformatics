import sys


def read_FASTA(file):
    DNAs = {}
    label = ''
    DNA = ''
    for line in file:
        if line[0] == '>':
            if label != '':
                DNAs[label] = DNA
                DNA = ''
            label = str(line[1:]).rstrip()
        else:
            DNA += line.rstrip()
    DNAs[label] = DNA
    return DNAs


def are_DNAs_connected(u, v):
    k = 3
    return u != v and u[-k:] == v[:k]


DNAs = read_FASTA(sys.stdin)
adjacency = []
for i in DNAs:
    for j in DNAs:
        if i != j and are_DNAs_connected(DNAs[i], DNAs[j]):
            adjacency.append((i, j))

for a in adjacency:
    print(a[0] + " " + a[1])