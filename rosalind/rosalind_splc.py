import sys
import rosalind

fasta = rosalind.read_multi_fasta(sys.stdin)
dna = fasta[0].sequence
introns = [f.sequence for f in fasta[1:]]

for intron in introns:
    dna = dna.replace(intron, '')

print(rosalind.rna_to_protein(rosalind.dna_to_rna(dna)))
