import sys
import rosalind

fasta = rosalind.read_fasta(sys.stdin)
dna = fasta.sequence

for i in range(4, 14, 2):
    for j in range(0, len(dna) - i + 1):
        chunk = dna[j:j+i]
        if chunk == rosalind.reverse_complement(chunk):
            print(j + 1, " ", i)