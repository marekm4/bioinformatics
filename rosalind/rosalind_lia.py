import re
import math
import scipy.special


class Organism:
    def __init__(self, genotype):
        pattern = re.compile("^(a){2}(b){2}$", re.IGNORECASE)
        if not pattern.match(genotype):
            raise ValueError("invalid genotype")
        t = list(genotype)
        if t[0] > t[1]:
            t[0], t[1] = t[1], t[0]
        if t[2] > t[3]:
            t[2], t[3] = t[3], t[2]
        self.genotype = ''.join(t)

    def possible_gamets(self):
        return [
            self.genotype[0]+self.genotype[2],
            self.genotype[0]+self.genotype[3],
            self.genotype[1]+self.genotype[2],
            self.genotype[1]+self.genotype[3],
        ]

    def __repr__(self):
        return self.genotype

    def __eq__(self, other):
        return self.genotype == other.genotype

    def __hash__(self):
        return self.genotype.__hash__()


class Cross:
    def __init__(self, o1, o2, probability = 1):
        self.o1 = o1
        self.o2 = o2
        self.possible_offsprings = {}
        o1_gamets = o1.possible_gamets()
        o2_gamets = o2.possible_gamets()
        for o1_gamet in o1_gamets:
            for o2_gamet in o2_gamets:
                offspring = Organism(o1_gamet[0] + o2_gamet[0] + o1_gamet[1] + o2_gamet[1])
                if offspring not in self.possible_offsprings:
                    self.possible_offsprings[offspring] = probability
                else:
                    self.possible_offsprings[offspring] += probability
        for offspring in self.possible_offsprings:
            self.possible_offsprings[offspring] /= 16

    def organism_probability(self, o):
        if o in self.possible_offsprings:
            return self.possible_offsprings[o]
        return 0

Tom = Organism("AaBb")
AaBb = Organism("AaBb")

# there is no point in calculating crosses, probability for AaBb is 0.25 in every generation
probability = p = .25

args = input().split(" ")
k = int(args[1])
n = math.pow(2, int(args[0]))

sum = 0
for i in range(k, int(n) + 1):
    sum += scipy.special.binom(n, i) * math.pow(p, i) * math.pow(1 - p, n - i)
print(sum)