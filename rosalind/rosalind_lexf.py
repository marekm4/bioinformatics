import itertools

alphabet = input().split(' ')
n = int(input())

products = list(itertools.product(alphabet, repeat=n))
for product in products:
    print(''.join([str(p) for p in product]))