import itertools

n = int(input())
perms = list(itertools.permutations(range(1, n+1)))
print(len(perms))
for perm in perms:
    print(' '.join([str(p) for p in perm]))