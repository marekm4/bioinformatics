def reverse_complement(dna):
    reverse = dna[::-1]
    complement = {
        'A': 'T',
        'C': 'G',
        'G': 'C',
        'T': 'A',
    }
    reversed_complement = ''
    for c in reverse:
        reversed_complement += complement[c]
    return reversed_complement


class Fasta:
    def __init__(self, name, sequence):
        self.name = name
        self.sequence = sequence

    def __repr__(self):
        return '{}: {}'.format(self.name, self.sequence)

    def __eq__(self, other):
        return self.name == other.name and self.sequence == other.sequence


def read_multi_fasta(file):
    sequences = []
    name = ''
    sequence = ''
    for line in file:
        line = line.rstrip()
        if line == '':
            continue
        elif line[0] == '>':
            if name != '':
                sequences.append(Fasta(name, sequence))
                name = ''
                sequence = ''
            name = str(line[1:])
        else:
            sequence += line
    sequences.append(Fasta(name, sequence))
    return sequences


def read_fasta(file):
    return read_multi_fasta(file)[0]

def dna_to_rna(dna):
    rna = ''
    for c in dna:
        if c == 'T':
            rna += 'U'
        else:
            rna += c
    return rna


def codon_to_protein(codon):
    codons = {
        'UUU': 'F',
        'CUU': 'L',
        'AUU': 'I',
        'GUU': 'V',
        'UUC': 'F',
        'CUC': 'L',
        'AUC': 'I',
        'GUC': 'V',
        'UUA': 'L',
        'CUA': 'L',
        'AUA': 'I',
        'GUA': 'V',
        'UUG': 'L',
        'CUG': 'L',
        'AUG': 'M',
        'GUG': 'V',
        'UCU': 'S',
        'CCU': 'P',
        'ACU': 'T',
        'GCU': 'A',
        'UCC': 'S',
        'CCC': 'P',
        'ACC': 'T',
        'GCC': 'A',
        'UCA': 'S',
        'CCA': 'P',
        'ACA': 'T',
        'GCA': 'A',
        'UCG': 'S',
        'CCG': 'P',
        'ACG': 'T',
        'GCG': 'A',
        'UAU': 'Y',
        'CAU': 'H',
        'AAU': 'N',
        'GAU': 'D',
        'UAC': 'Y',
        'CAC': 'H',
        'AAC': 'N',
        'GAC': 'D',
        'CAA': 'Q',
        'AAA': 'K',
        'GAA': 'E',
        'CAG': 'Q',
        'AAG': 'K',
        'GAG': 'E',
        'UGU': 'C',
        'CGU': 'R',
        'AGU': 'S',
        'GGU': 'G',
        'UGC': 'C',
        'CGC': 'R',
        'AGC': 'S',
        'GGC': 'G',
        'CGA': 'R',
        'AGA': 'R',
        'GGA': 'G',
        'UGG': 'W',
        'CGG': 'R',
        'AGG': 'R',
        'GGG': 'G',
    }
    return codons[codon]


def is_stop_codon(codon):
    stop_codons = ['UAA', 'UAG', 'UGA']
    return codon in stop_codons


def is_start_codon(codon):
    return codon == 'AUG'


def rna_to_protein(rna):
    proteins = ''
    started = False
    stopped = False
    for i in range(0, len(rna), 3):
        codon = rna[i:i + 3]
        if len(codon) != 3:
            raise ValueError('codon is to short')
        if is_start_codon(codon):
            started = True
        if started:
            if is_stop_codon(codon):
                stopped = True
                break
            proteins += codon_to_protein(codon)
    if not stopped:
        raise ValueError('protein could not be formed')
    return proteins
