args = input().split(" ")
n = int(args[0])
k = int(args[1])

population = [
    {
        'newborn': 1,
        'reproductive': 0,
    },
]

for i in range(1, n):
    last_generation = population[i-1]
    newborn = last_generation['reproductive'] * k
    reproductive = last_generation['reproductive'] + last_generation['newborn']
    population.append({
        'newborn': newborn,
        'reproductive': reproductive
    })

print(population[n-1]['newborn'] + population[n-1]['reproductive'])