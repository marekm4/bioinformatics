import unittest
import rosalind

class Test(unittest.TestCase):
    def test_reverse_complement(self):
        dna = 'ACTA'
        self.assertEqual('TAGT', rosalind.reverse_complement(dna))

    def test_reverse_complement_transitive(self):
        dna = 'ACTAGG'
        self.assertEqual(dna, rosalind.reverse_complement(rosalind.reverse_complement(dna)))

    def test_read_fasta(self):
        file = open('fasta.txt', 'r')
        sequences = rosalind.read_multi_fasta(file)
        file.close()
        self.assertEqual([
            rosalind.Fasta('Taxon1', 'CCTGCGGAAGATCGGCACTAGAATAGCCAGAACCGTTTCTCTGAGGCTTCCGGCCTTCCCTCCCACTAATAATTCTGAGG'),
            rosalind.Fasta('Taxon2', 'CCATCGGTAGCGCATCCTTAGTCCAATTAAGTCCCTATCCAGGCGCTCCGCCGAAGGTCTATATCCATTTGTCAGCAGACACGC'),
            rosalind.Fasta('Taxon3', 'CCACCCTCGTGGTATGGCTAGGCATTCAGGAACCGGAGAACGCTTCAGACCAGCCCGGACTGGGAACCTGCGGGCAGTAGGTGGAAT'),
        ], sequences)

    def test_dna_to_rna(self):
        dna = 'ACTAGGTATG'
        self.assertEqual('ACUAGGUAUG', rosalind.dna_to_rna(dna))

    def test_rna_to_protein(self):
        rna = 'GGGAUGCGUCGUUGA'
        self.assertEqual('MRR', rosalind.rna_to_protein(rna))

if __name__ == '__main__':
    unittest.main()