import sys

even = False
for line in sys.stdin:
    if even:
        print(line.rstrip())
    even = not even