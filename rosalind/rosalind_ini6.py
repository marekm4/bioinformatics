text = input()
words = {}

for word in text.split(" "):
    if word in words:
        words[word] += 1
    else:
        words[word] = 1

for word in words:
    print(word + " " + str(words[word]))