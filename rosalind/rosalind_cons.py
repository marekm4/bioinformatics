import sys
import rosalind


DNAs = rosalind.read_multi_fasta(sys.stdin)
n = len(DNAs[0].sequence)
profile = {}
for na in 'ACGT':
    profile[na] = []
    for i in range(0, n):
        profile[na].append(0)

for DNA in DNAs:
    for i in range(0, n):
        profile[DNA.sequence[i]][i] += 1

consensus = ''
for i in range(0, n):
    max = profile['A'][i]
    max_na = 'A'
    for na in 'CGT':
        if profile[na][i] > max:
            max = profile[na][i]
            max_na = na
    consensus += max_na

print(consensus)
for na in 'ACGT':
    print('{}: {}'.format(na, ' '.join(map(str, profile[na]))))