args = input().split(" ")
genotypes = {
    'AA-AA': int(args[0]),
    'AA-Aa': int(args[1]),
    'AA-aa': int(args[2]),
    'Aa-Aa': int(args[3]),
    'Aa-aa': int(args[4]),
    'aa-aa': int(args[5]),
}

dominant_phenotype_probability = {
    'AA-AA': 1.,
    'AA-Aa': 1.,
    'AA-aa': 1.,
    'Aa-Aa': .75,
    'Aa-aa': .5,
    'aa-aa': 0.,
}

sum = 0.
for g in genotypes:
    sum += genotypes[g] * dominant_phenotype_probability[g]

offspring = 2
print(sum * offspring)